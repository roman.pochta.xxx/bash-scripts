#!/bin/sh
#TMP_LOG must be the same in cron too - /root/./dump_backup.sh 124 3p.x.inteniumstudio.com | tee /root/dump_backup.tmp

TMP_LOG=/root/dump_backup.tmp
LOG=dump_backup.log
BK_DIR=/vz/dump/dump_backups

CT_ID=$1
CT_HOSTNAME=$2
CT_ID_LOG=$CT_ID.$(date +"%Y-%m-%d_%H-%M").$LOG
echo "=Start backup $CT_ID "$(date +"%Y-%m-%d_%H-%M-%S")
echo "==START vzdump"
time=$(date +%s)
vzdump --suspend --bwlimit 250000 --dumpdir /vz/dump/dump_backups/ $1
echo "==END vzdump"
DUMP_TIME=$(($(date +%s)-$time))
dump_file_name_link=$(cat $TMP_LOG | grep "creating archive" | tr -d \' | awk '{print $4}')
mv $dump_file_name_link $BK_DIR/vzdmp-CTID$CT_ID-$CT_HOSTNAME-$(date +"%Y-%m-%d_%H-%M").tar

#optional synchronization

echo "=Start rsync $CT_ID "$(date +"%Y-%m-%d_%H-%M-%S")
time=$(date +%s)
rsync -a --bwlimit=45000 --del $BK_DIR/ backupsync@ns3201069.eu:/storage1/backups/containers_2
COPY_TIME=$(($(date +%s)-$time))
echo "=END rsync $CT_ID "$(date +"%Y-%m-%d_%H-%M-%S")
echo "DUMP_TIME: "$DUMP_TIME" s"
echo "COPY_TIME: "$COPY_TIME" s"
cp $TMP_LOG $BK_DIR/$CT_ID_LOG
scp $BK_DIR/$CT_ID_LOG backupsync@ns3201069.eu:/storage1/backups/containers_2
