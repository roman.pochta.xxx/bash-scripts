#!/bin/bash
#07.02.2024
# -locked="false" - this runners are available to all projects in GitLab

DIR="ci-cd"
RUNNERS=3

GITLAB_TOKEN="xxxxxxxxxxxx"
GITLAB_URL="https://servername.com/"

# Unregister all runners
for CN in $(docker ps | grep "${DIR}-" | awk '{print $1}'); do
    echo "Unregister runner ${CN}..."
    docker exec -ti ${CN} gitlab-runner unregister --url ${GITLAB_URL} --token ${GITLAB_TOKEN} --all-runners
    echo "Sleep ..." && sleep 1
done

echo -n

# Down compose
test -f ./${DIR}/docker-compose.yml && docker-compose -f ./${DIR}/docker-compose.yml down

echo -n

# Clear && and create dir
test -d "./${DIR}/" && rm -rf "./${DIR}/"

# Create runners directory
mkdir "./${DIR}/"

# Start docker compose file
cat << EOF > ./${DIR}/docker-compose.yml
version: '2'
services:
EOF

# Append compose files
for N in $(seq ${RUNNERS}); do
cat << EOF >> ./${DIR}/docker-compose.yml
  git-${DIR}-${N}:
    privileged: true
    container_name: git-${DIR}-${N}
    image: gitlab/gitlab-runner:alpine
    volumes:
      - "./data/cache:/cache:rw"
      - "./data/git-${DIR}-${N}:/etc/gitlab-runner"
      - "/var/run/docker.sock:/var/run/docker.sock"
    restart: always

EOF
done

# Compose UP
docker-compose -f ./${DIR}/docker-compose.yml pull
docker-compose -f ./${DIR}/docker-compose.yml up -d

docker ps | grep "git-${DIR}-" | awk '{print $1}'

# Declare counter
declare -i COUNTER=0

echo "Sleep ..." && sleep 3

# Register all runers
for CN in $(docker ps | grep "git-${DIR}-" | awk '{print $1}'); do
    COUNTER=$(expr ${COUNTER} + 1)

    docker exec -ti ${CN} gitlab-runner register \
        --non-interactive \
        --url ${GITLAB_URL} \
        --registration-token ${GITLAB_TOKEN} \
        --description "${DIR}-${COUNTER}" \
        --docker-image "docker:stable" \
        --executor "docker" \
        --tag-list "docker" \
        --run-untagged \
        --locked="false" \
        --docker-privileged="false" \
        --docker-volumes="/var/run/docker.sock:/var/run/docker.sock" \
        --docker-volumes="/root/data/cache:/cache" \
        --cache-dir="/cache"

    echo "Sleep ..." && sleep 2
done
