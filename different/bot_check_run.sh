#!/bin/bash
# Делал тестовое
# Проверка запущен ли бот. Если нет, то запустить tmux сессию
bot=$1
echo "Robot name: $bot"

#Есть ли запущенный bot
BOT_STATUS=$(ps -Af | grep $bot | grep -v grep | wc -l)
echo "BOT_STATUS - $BOT_STATUS"

if [ $BOT_STATUS == 0 ];
#робот не запущен
then
tmux new-session -d -s ${bot} -n win1
tmux send-keys -t '${bot}' 'source ~/.bashrc' Enter
tmux send-keys -t '${bot}' 'bot ${bot}' Enter
else
tmux attach-session -t '${bot}'
