#!/bin/bash
DISTROS=$(whiptail --title "Test Checklist Dialog" --checklist \
"Choose preferred Linux distros" 15 60 4 \
debian "Venerable Debian" ON \
ubuntu "Popular Ubuntu" OFF \
cento "Stable CentOS" ON \
"mint" "Rising Star Mint" OFF 3>&1 1>&2 2>&3)

exitstatus=$?
if [ $exitstatus = 0 ]; then
   # echo "Your favorite distros are:" $DISTROS
        echo $DISTROS | awk '{print $0}'
else
    echo "You chose Cancel."
fi

#ct_id=`echo $machine | awk 'BEGIN{FS="|"} {print $9}'`


IFS=' ' read -ra my_array <<< "${DISTROS}"

# Печать разделенной строки
s=1
for i in "${my_array[@]}"
do
  	echo $i
        echo $s
        (( s++ ))
done

echo -e "==================================\n"

case "${DISTROS}" in
  *debian*)
    # Do stuff
    echo "OS is Debian"
    ;;
esac
