#!/bin/bash
MYSQL_PASSWD="xxxxxxxxxx"

echo -e "Start script\n"

mysql -uroot -p$MYSQL_PASSWD -e "show databases" | \
grep -v Database | \
grep -v mysql| \
grep -v information_schema | \
grep -v performance_schema | \
grep -v sys | \
gawk '{print "drop database " $1 ";select sleep(0.1);"}' | \
mysql -uroot -p$MYSQL_PASSWD

echo -e "\n=========\n End script \n========="
